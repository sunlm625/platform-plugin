package org.fujie.plugin.cloud.comm.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author slm
 * @description 实人认证异常类
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class IdCardScanException extends BaseException {


    private static final Integer FAIL_CODE = 5001;

    private Integer code;

    private String message;

    /**
     * 异常详情，用于详细日志记录
     */
    private String traceDetail;

    public IdCardScanException() {
        this.code = FAIL_CODE;
    }

    public IdCardScanException setCode(Integer code) {
        this.code = code;
        return this;
    }

    public IdCardScanException setMessage(String message) {
        this.message = message;
        return this;
    }

    public IdCardScanException setTraceDetail(String traceDetail) {
        this.traceDetail = traceDetail;
        return this;
    }
}
