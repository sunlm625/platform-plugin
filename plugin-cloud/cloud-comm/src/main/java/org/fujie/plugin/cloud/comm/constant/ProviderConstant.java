package org.fujie.plugin.cloud.comm.constant;

import lombok.Data;

/**
 * @author slm
 * @description 云服务商常量
 */
@Data
public class ProviderConstant {

    public static final String ALI_CLOUD = "alibaba";
    public static final String TENCENT_CLOUD = "tencent";
    public static final String BAIDU_CLOUD = "baidu";

    private ProviderConstant() {
    }

}
