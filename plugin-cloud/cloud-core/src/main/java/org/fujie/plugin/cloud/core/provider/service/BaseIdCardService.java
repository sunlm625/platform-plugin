package org.fujie.plugin.cloud.core.provider.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.fujie.plugin.cloud.core.model.RecognizedIdCardScanRes;

/**
 * @author slm
 * @description 身份证识别服务接口
 * 定义云服务插件功能
 */

public interface BaseIdCardService {

    /**
     * 获取供应商标识
     */
    String getSupplier();

    /**
     * @param frontUrl 国徽面图片网络地址
     * @author sunliming
     * @description 身份证国徽面识别
     */
    RecognizedIdCardScanRes recognizeIdFrontCard(String frontUrl) throws JsonProcessingException;

    /**
     * @param backUrl 人像面图片地址
     * @author sunliming
     * @description 身份证人像面识别
     */
    RecognizedIdCardScanRes recognizeIdBackCard(String backUrl) throws JsonProcessingException;
}
