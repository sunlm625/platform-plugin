package org.fujie.plugin.cloud.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * @author slm
 * @description 身份证识别结果
 */
@Data
public class RecognizedIdCardScanRes implements Serializable {

    /**
     * 身份证号码
     */
    private String certNo;
    /**
     * 姓名
     */
    private String certName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 民族
     */
    private String ethnicity;
    /**
     * 出生日期
     */
    private String birthDate;
    /**
     * 地址
     */
    private String address;
    /**
     * 签发机关
     */
    private String issueAuthority;
    /**
     * 有效期时间
     */
    private String validPeriod;

    /**
     * 有效期开始时间
     */
    private String startDate;
    /**
     * 有效期结束时间
     */
    private String endDate;

}
