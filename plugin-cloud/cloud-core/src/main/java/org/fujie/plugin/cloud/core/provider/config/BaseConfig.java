package org.fujie.plugin.cloud.core.provider.config;

import org.fujie.plugin.cloud.core.enumd.ConfigType;

/**
 * @author slm
 * @description 顶级配置接口
 */

public interface BaseConfig {

    /**
     * 配置源类型
     */
    ConfigType configType = ConfigType.YAML;

    /**
     * 获取供应商
     */
    String getSupplier();
}
