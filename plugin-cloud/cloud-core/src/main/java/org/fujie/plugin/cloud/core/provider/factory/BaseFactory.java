package org.fujie.plugin.cloud.core.provider.factory;

import org.fujie.plugin.cloud.core.provider.config.BaseConfig;
import org.fujie.plugin.cloud.core.provider.service.BaseIdCardService;

/**
 * @author slm
 * @description 顶级工厂接口
 */

public interface BaseFactory<S extends BaseIdCardService, C extends BaseConfig> {

    /**
     * 创建实现对象
     *
     * @param c 云服务配置对象
     * @return 云服务实现对象
     */
    S createServer(C c);

    /**
     * 获取配置类
     *
     * @return 配置类
     */
    Class<C> getConfigClass();

    /**
     * 获取供应商
     *
     * @return 供应商
     */
    String getSupplier();
}
