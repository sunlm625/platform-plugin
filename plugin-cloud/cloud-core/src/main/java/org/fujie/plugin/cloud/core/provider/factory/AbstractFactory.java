package org.fujie.plugin.cloud.core.provider.factory;

import lombok.Getter;
import org.fujie.plugin.cloud.core.provider.config.BaseConfig;
import org.fujie.plugin.cloud.core.provider.service.BaseIdCardService;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author slm
 * @description 抽象工厂接口, 云服务商接口统一继承
 */
@Getter
public abstract class AbstractFactory<S extends BaseIdCardService, C extends BaseConfig> implements BaseFactory<S, C> {

    /**
     * -- GETTER --
     *  Retrieves the configuration class type.
     *
     * @return The configuration class.
     */
    private final Class<C> configClass;

    @SuppressWarnings("unchecked")
    protected AbstractFactory() {
        Type genericSuperclass = getClass().getGenericSuperclass();

        if (!(genericSuperclass instanceof ParameterizedType)) {
            throw new IllegalArgumentException("Class is not parameterized");
        }

        Type[] typeArguments = ((ParameterizedType) genericSuperclass).getActualTypeArguments();

        if (typeArguments.length <= 1 || !(typeArguments[1] instanceof Class<?>)) {
            throw new IllegalArgumentException("Expected the second generic parameter to be a Class");
        }

        this.configClass = (Class<C>) typeArguments[1];
    }

}
