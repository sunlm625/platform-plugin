package org.fujie.plugin.cloud.core.provider.config;

import lombok.Data;

/**
 * @author slm
 * @description
 */
@Data
public abstract class ProviderConfig implements BaseConfig {

    /**
     * key
     */
    private String accessKeyId;

    /**
     * 密匙
     */
    private String accessKeySecret;

    /**
     * 区域
     */
    private String endpoint;

}
