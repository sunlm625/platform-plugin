package org.fujie.plugin.cloud.core.enumd;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author slm
 * @description 配置类型枚举, 当前仅支持 yaml 文件获取配置，后续增加
 * 其他方式的扩展
 */
@Getter
@AllArgsConstructor
public enum ConfigType {

    YAML("yaml");

    private final String type;
}
