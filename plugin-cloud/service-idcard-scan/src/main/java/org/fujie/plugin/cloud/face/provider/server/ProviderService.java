package org.fujie.plugin.cloud.face.provider.server;

import org.fujie.plugin.cloud.core.provider.config.BaseConfig;
import org.fujie.plugin.cloud.core.provider.service.BaseIdCardService;

/**
 * @author slm
 * @description 各云服务商服务抽象
 */
public abstract class ProviderService<C extends BaseConfig> implements BaseIdCardService {

    private final C config;

    protected ProviderService(C config) {
        this.config = config;
    }

    protected C getConfig() {
        return config;
    }

}
