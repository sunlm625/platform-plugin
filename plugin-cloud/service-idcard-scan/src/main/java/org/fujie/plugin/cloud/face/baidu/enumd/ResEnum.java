package org.fujie.plugin.cloud.face.baidu.enumd;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author slm
 * @description
 */
@Getter
@AllArgsConstructor
public enum ResEnum {

    NORMAL("normal", "识别正常"),
    REVERSED_SIDE("reversed_side", "身份证正反面颠倒"),
    NON_ID_CARD("non_idcard", "上传的图片中不包含身份证"),
    BLURRED("blurred", "身份证模糊"),
    OTHER_TYPE_CARD("other_type_card", "其他类型证照"),
    OVER_EXPOSURE("over_exposure", "身份证关键字段反光或过曝"),
    OVER_DARK("over_dark", "身份证欠曝（亮度过低）"),
    UNKNOWN("unknown", "未知状态失败");

    private final String status;

    private final String desc;
}
