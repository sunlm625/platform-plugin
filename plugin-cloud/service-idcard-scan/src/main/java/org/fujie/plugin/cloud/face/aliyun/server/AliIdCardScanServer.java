package org.fujie.plugin.cloud.face.aliyun.server;

import cn.hutool.core.util.ObjectUtil;
import com.aliyun.ocr_api20210707.Client;
import com.aliyun.ocr_api20210707.models.RecognizeIdcardRequest;
import com.aliyun.ocr_api20210707.models.RecognizeIdcardResponseBody;
import com.aliyun.teaopenapi.models.Config;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.fujie.plugin.cloud.comm.constant.ProviderConstant;
import org.fujie.plugin.cloud.comm.exception.IdCardScanException;
import org.fujie.plugin.cloud.comm.util.JsonUtil;
import org.fujie.plugin.cloud.core.model.RecognizedIdCardScanRes;
import org.fujie.plugin.cloud.face.aliyun.config.AliConfig;
import org.fujie.plugin.cloud.face.aliyun.convert.AliIdCardConvert;
import org.fujie.plugin.cloud.face.provider.server.ProviderService;

/**
 * @author slm
 * @description
 */
public class AliIdCardScanServer extends ProviderService<AliConfig> {

    private final Client client;

    @SneakyThrows(Exception.class)
    public AliIdCardScanServer(AliConfig aliConfig) {
        super(aliConfig);

        Config config = new Config()
                .setAccessKeyId(aliConfig.getAccessKeyId())
                .setAccessKeySecret(aliConfig.getAccessKeySecret())
                .setEndpoint(aliConfig.getEndpoint());
        this.client = new Client(config);
    }

    /**
     * 获取供应商标识
     *
     * @return
     */
    @Override
    public String getSupplier() {
        return ProviderConstant.ALI_CLOUD;
    }

    /**
     * @param frontUrl 国徽面图片网络地址
     * @author sunliming
     * @description 身份证国徽面识别
     */
    @Override
    public RecognizedIdCardScanRes recognizeIdFrontCard(String frontUrl) throws JsonProcessingException {

        JsonNode frontNode = parseIdCard(frontUrl);
        JsonNode data = frontNode.get("data").get("face").get("data");
        return AliIdCardConvert.ofFront(data);

    }

    /**
     * @param backUrl 人像面图片地址
     * @author sunliming
     * @description 身份证人像面识别
     */
    @Override
    public RecognizedIdCardScanRes recognizeIdBackCard(String backUrl) throws JsonProcessingException {

        JsonNode backNode = parseIdCard(backUrl);
        JsonNode data = backNode.get("data").get("back").get("data");
        return AliIdCardConvert.ofBack(data);

    }

    private JsonNode parseIdCard(String url) throws JsonProcessingException {

        RecognizeIdcardResponseBody frontRes = null;
        try {
            frontRes = client.recognizeIdcard(new RecognizeIdcardRequest()
                    .setUrl(url)).getBody();
        } catch (Exception e) {
            throw new IdCardScanException().setMessage("阿里云服务访问失败:%s".formatted(e.getMessage()));
        }
        String frontResCode = frontRes.getCode();
        if (ObjectUtil.isNotNull(frontResCode)) {
            throw new IdCardScanException().setMessage("身份证解析失败:%s".formatted(frontRes.message));
        }

        ObjectMapper objectMapper = JsonUtil.getObjectMapper();
        return objectMapper.readTree(frontRes.getData());

    }
}
