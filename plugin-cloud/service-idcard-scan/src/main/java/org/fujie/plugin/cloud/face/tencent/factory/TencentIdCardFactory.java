package org.fujie.plugin.cloud.face.tencent.factory;

import org.fujie.plugin.cloud.comm.constant.ProviderConstant;
import org.fujie.plugin.cloud.core.provider.factory.AbstractFactory;
import org.fujie.plugin.cloud.face.tencent.config.TencentConfig;
import org.fujie.plugin.cloud.face.tencent.server.TencentIdCardServer;

/**
 * @author slm
 * @description
 */
public class TencentIdCardFactory extends AbstractFactory<TencentIdCardServer, TencentConfig> {

    private static final TencentIdCardFactory INSTANCE = new TencentIdCardFactory();

    public static TencentIdCardFactory getInstance(){
        return INSTANCE;
    }
    /**
     * 创建实现对象
     *
     * @param tencentConfig 云服务配置对象
     * @return 云服务实现对象
     */
    @Override
    public TencentIdCardServer createServer(TencentConfig tencentConfig) {

        return new TencentIdCardServer(tencentConfig);
    }

    /**
     * 获取供应商
     *
     * @return 供应商
     */
    @Override
    public String getSupplier() {
        return ProviderConstant.TENCENT_CLOUD;
    }
}
