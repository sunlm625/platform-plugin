package org.fujie.plugin.cloud.face.tencent.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.fujie.plugin.cloud.comm.constant.ProviderConstant;
import org.fujie.plugin.cloud.core.provider.config.ProviderConfig;

/**
 * @author slm
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TencentConfig extends ProviderConfig {

    /**资源地域*/
    private String region;

    /**
     * 获取供应商
     */
    @Override
    public String getSupplier() {
        return ProviderConstant.TENCENT_CLOUD;
    }
}
