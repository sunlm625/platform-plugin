package org.fujie.plugin.cloud.face.provider.config;

import org.fujie.plugin.cloud.face.provider.init.IdCardInitializer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author slm
 * @description 身份证识别配置类，加载 bean 容器
 */
public class IdCardScanConfig {

    @Bean
    @ConfigurationProperties(prefix = "cloud.server-name")
    @ConditionalOnProperty(prefix = "cloud", name = "config-type", havingValue = "yaml")
    protected Map<String, Map<String, Object>> blends() {
        return new LinkedHashMap<>();
    }

    @Bean
    @ConditionalOnProperty(prefix = "cloud", name = "config-type", havingValue = "yaml")
    protected IdCardInitializer smsBlendsInitializer(Map<String, Map<String, Object>> servers) {
        return new IdCardInitializer(servers);
    }

}
