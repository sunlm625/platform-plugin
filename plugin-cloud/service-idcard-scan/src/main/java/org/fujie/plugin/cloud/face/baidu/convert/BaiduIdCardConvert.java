package org.fujie.plugin.cloud.face.baidu.convert;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.fujie.plugin.cloud.core.model.RecognizedIdCardScanRes;

import java.util.Optional;

/**
 * @author slm
 * @description
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BaiduIdCardConvert {

    public static RecognizedIdCardScanRes ofFront(JsonNode node) {

        RecognizedIdCardScanRes res = new RecognizedIdCardScanRes();

        Optional.ofNullable(node.get("公民身份号码"))
                .map(n -> n.get("words"))
                .map(JsonNode::asText).ifPresent(res::setCertNo);

        Optional.ofNullable(node.get("姓名"))
                .map(n -> n.get("words"))
                .map(JsonNode::asText).ifPresent(res::setCertName);

        Optional.ofNullable(node.get("性别"))
                .map(n -> n.get("words"))
                .map(JsonNode::asText).ifPresent(res::setSex);

        Optional.ofNullable(node.get("民族"))
                .map(n -> n.get("words"))
                .map(JsonNode::asText).ifPresent(res::setEthnicity);

        Optional.ofNullable(node.get("出生"))
                .map(n -> n.get("words"))
                .map(JsonNode::asText).ifPresent(res::setBirthDate);

        Optional.ofNullable(node.get("住址"))
                .map(n -> n.get("words"))
                .map(JsonNode::asText).ifPresent(res::setAddress);

        return res;
    }

    public static RecognizedIdCardScanRes ofBack(JsonNode node) {

        RecognizedIdCardScanRes res = new RecognizedIdCardScanRes();

        Optional.ofNullable(node.get("失效日期"))
                .map(n -> n.get("words"))
                .map(JsonNode::asText).ifPresent(res::setCertNo);

        Optional.ofNullable(node.get("签发机关"))
                .map(n -> n.get("words"))
                .map(JsonNode::asText).ifPresent(res::setCertName);

        Optional.ofNullable(node.get("签发日期"))
                .map(n -> n.get("words"))
                .map(JsonNode::asText).ifPresent(res::setSex);

        return res;

    }

}
