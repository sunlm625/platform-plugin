package org.fujie.plugin.cloud.face.aliyun.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.fujie.plugin.cloud.comm.constant.ProviderConstant;
import org.fujie.plugin.cloud.core.provider.config.ProviderConfig;

/**
 * @author slm
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AliConfig extends ProviderConfig {

    /**
     * 获取供应商
     */
    @Override
    public String getSupplier() {
        return ProviderConstant.ALI_CLOUD;
    }
}
