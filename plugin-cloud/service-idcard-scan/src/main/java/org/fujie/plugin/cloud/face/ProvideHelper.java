package org.fujie.plugin.cloud.face;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.fujie.plugin.cloud.comm.exception.IdCardScanException;
import org.fujie.plugin.cloud.core.provider.config.BaseConfig;
import org.fujie.plugin.cloud.core.provider.factory.BaseFactory;
import org.fujie.plugin.cloud.core.provider.factory.FactoryHolder;
import org.fujie.plugin.cloud.core.provider.service.BaseIdCardService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author slm
 * @description 服务调用入口
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProvideHelper {

    private static final Map<String, BaseIdCardService> serviceMap = new ConcurrentHashMap<>();

    /**
     * @author sunliming
     * @description 注册服务
     */
    public static <T extends BaseConfig> void register(T config) {

        BaseFactory<? extends BaseIdCardService, T> factory = (BaseFactory<? extends BaseIdCardService, T>) FactoryHolder.getFactory(config.getSupplier());

        if (factory == null) {
            throw new IdCardScanException().setMessage("不支持当前供应商配置");
        }
        serviceMap.put(config.getSupplier(), factory.createServer(config));
    }

    /**
     * @author sunliming
     * @description 获取服务
     */
    public static BaseIdCardService getService(String supplier) {

        return serviceMap.get(supplier);
    }

}
