package org.fujie.plugin.cloud.face.baidu.factory;

import org.fujie.plugin.cloud.comm.constant.ProviderConstant;
import org.fujie.plugin.cloud.core.provider.factory.AbstractFactory;
import org.fujie.plugin.cloud.face.baidu.config.BaiduConfig;
import org.fujie.plugin.cloud.face.baidu.server.BaiduIdCardScanServer;

/**
 * @author slm
 * @description
 */
public class BaiduIdCardFactory extends AbstractFactory<BaiduIdCardScanServer, BaiduConfig> {

    public static final BaiduIdCardFactory INSTANCE = new BaiduIdCardFactory();

    public static BaiduIdCardFactory getInstance() {
        return INSTANCE;
    }

    /**
     * 创建实现对象
     *
     * @param baiduConfig 云服务配置对象
     * @return 云服务实现对象
     */
    @Override
    public BaiduIdCardScanServer createServer(BaiduConfig baiduConfig) {
        return new BaiduIdCardScanServer(baiduConfig);
    }

    /**
     * 获取供应商
     *
     * @return 供应商
     */
    @Override
    public String getSupplier() {
        return ProviderConstant.BAIDU_CLOUD;
    }
}
