package org.fujie.plugin.cloud.face.baidu.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.fujie.plugin.cloud.comm.constant.ProviderConstant;
import org.fujie.plugin.cloud.core.provider.config.ProviderConfig;

/**
 * @author slm
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BaiduConfig extends ProviderConfig {

    private String appId;

    /**
     * 获取供应商
     */
    @Override
    public String getSupplier() {
        return ProviderConstant.BAIDU_CLOUD;
    }
}
