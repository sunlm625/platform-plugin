package org.fujie.plugin.cloud.face.aliyun.factory;

import org.fujie.plugin.cloud.comm.constant.ProviderConstant;
import org.fujie.plugin.cloud.core.provider.factory.AbstractFactory;
import org.fujie.plugin.cloud.face.aliyun.config.AliConfig;
import org.fujie.plugin.cloud.face.aliyun.server.AliIdCardScanServer;

/**
 * @author slm
 * @description
 */
public class AliIdCardFactory extends AbstractFactory<AliIdCardScanServer, AliConfig> {

    public static final AliIdCardFactory INSTANCE = new AliIdCardFactory();

    public static AliIdCardFactory getInstance() {
        return INSTANCE;
    }

    /**
     * 创建实现对象
     *
     * @param aliConfig 云服务配置对象
     * @return 云服务实现对象
     */
    @Override
    public AliIdCardScanServer createServer(AliConfig aliConfig) {
        return new AliIdCardScanServer(aliConfig);
    }

    /**
     * 获取供应商
     *
     * @return 供应商
     */
    @Override
    public String getSupplier() {
        return ProviderConstant.ALI_CLOUD;
    }
}
