# Platform-Plugin

`Platform-Plugin` 是一个基于插件化的 Java
库，其目标是为开发者轻松地提供和集成多样化的业务能力。无论是需要连接的云服务，还是业务中常见的功能组件，`Platform-Plugin`
都为你提供了方便快捷的实现。

## 技术选型

| 框架 |  版本  |
|:--:|:----:|
| jdk  |  17  |
|springboot|3.x|
|hutool|5.8.22|

## 模块架构说明

```
Platform-Plugin
|
|-- 核心模块 (Core Module)[待抽取]
|   |-- Utils: 提供各种工具类和辅助功能，使得整个库更为高效和健壮。
|   |-- Base Interface: 这是所有插件和组件的蓝本，确保了统一性和可扩展性。
|
|-- platform-example[单元测试模块]
|
|-- plugin-cloud[云服务插件]
|   |-- cloud-bom: 模块统一依赖管理
|   |-- cloud-comm: 公共依赖模块
|   |-- cloud-core: 云服务模块核心功能，插件和组件的蓝本，确保了统一性和可扩展性。
|   |-- service-idcard-scan: 身份证识别插件
|
|-- 业务组件模块 (Business Component Module) [计划中]
    |-- RBAC: 一个强大的角色、权限和用户管理系统，提供灵活的权限配置方式。
    |-- Authentication: 安全的用户认证模块，支持多种登录方式。
```

### 核心模块 (Core Module)

该模块是整个库的心脏，它提供了所有必要的工具和基本接口，确保了整个库的运行效率和稳定性。待完善后优化拆分

### 云服务模块 (plugin-cloud)

为了解决与各种云服务通信的复杂性，我们为每个主流的云服务提供了专门的接入模块。这些模块简化了API的调用，使得开发者可以专注于实际的业务逻辑。

### 业务组件模块 (Business Component Module) [计划中]

我们认识到开发者在业务开发中经常需要使用到的功能组件，如用户管理、权限控制等。因此，我们计划在未来版本中引入这些常用的业务组件，以满足开发者的需求。

## 功能说明

目前，我们已经实现了与多个云服务的集成，特别是身份证 OCR 识别功能。无论你选择哪个云服务平台，都可以通过我们提供的统一API进行身份证识别。

## 使用说明

### 安装【maven】

```
    <dependency>
        <groupId>org.fujie.plugin.cloud</groupId>
        <artifactId>service-idcard-scan</artifactId>
        <version>查询使用最新版本号</version>
    </dependency>
```

### 示例

```java
import platform_plugin.*;

public class Demo {
    public static void main(String[] args) {
        /*网络图片地址*/
        String url = "xxx";
        /*获取身份证人像面解析结果*/
        RecognizedIdCardScanRes res = ProvideHelper.getService(ProviderConstant.ALI_CLOUD)
                .recognizeIdFrontCard(url);

    }
}
```

## 后续规划

- 集成更多的云服务平台。
- 开发并完善业务组件模块。
- 优化代码，提高效率和稳定性。

## 许可证

`Platform-Plugin` 采用 [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) 许可证。

### Apache License 2.0 的主要特点：

1. **商业使用**：该许可证允许用户商业使用、修改、分发和授权原作品。

2. **分发**：你可以分发原始或修改后的作品，但必须提供明确的许可证声明和版权声明。

3. **修改**：你可以修改源代码并分发，但必须为修改后的代码提供明确的许可证声明和版权声明。

4. **专利权**：此许可证为版权持有人和贡献者提供了明确的专利权许可。

5. **商标权**：此许可证不授予使用任何作者或版权持有人的名字、商标或其他识别标志的权利。

6. **责任免除**：此许可证的软件是"按原样"提供的，不提供任何形式的明示或暗示的担保。

建议在使用`Platform-Plugin` 或任何其他采用Apache 2.0许可的软件时，仔细阅读和理解许可证的完整文本。