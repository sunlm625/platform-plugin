package org.fujie.platform.example.boot;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.fujie.plugin.cloud.comm.constant.ProviderConstant;
import org.fujie.plugin.cloud.comm.util.JsonUtil;
import org.fujie.plugin.cloud.core.model.RecognizedIdCardScanRes;
import org.fujie.plugin.cloud.face.ProvideHelper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
* @author sunliming
* @description 身份证识别单元测试
*/
@SpringBootTest
@Slf4j
class IdCardScanApplicationTests {

    @Test
    void contextLoads() throws JsonProcessingException {

        RecognizedIdCardScanRes res = ProvideHelper.getService(ProviderConstant.ALI_CLOUD).recognizeIdFrontCard("https://www.danyuanyuan.com/dyy-data/2023/07/15/7897677e88d8483e926ec8a47998492a.jpg");
        log.info(JsonUtil.toJsonString(res));
    }



}
